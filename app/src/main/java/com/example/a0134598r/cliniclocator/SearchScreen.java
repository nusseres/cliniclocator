package com.example.a0134598r.cliniclocator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;


public class SearchScreen extends Activity {

    RadioGroup rg;
    ArrayAdapter<CharSequence> adpt;
    Spinner reg,neighbour;
    RadioButton estate_name,specific_loc,rb,current_loc;
    EditText address;
    Button find;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_clinic);
        rg  = (RadioGroup)findViewById(R.id.grp_region);
        reg = (Spinner)findViewById(R.id.region);
        neighbour = (Spinner)findViewById(R.id.neighbour);
        estate_name = (RadioButton)findViewById(R.id.estate_name);
        current_loc = (RadioButton)findViewById(R.id.current_loc);
        current_loc.isChecked();
        specific_loc =(RadioButton)findViewById(R.id.specific_loc);
        address = (EditText)findViewById(R.id.address);
        adpt= ArrayAdapter.createFromResource(this,R.array.regions,R.layout.support_simple_spinner_dropdown_item);
        adpt.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        reg.setAdapter(adpt);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                rb = (RadioButton)findViewById(checkedId);

                if(rb.getId()==estate_name.getId()){
                    reg.setVisibility(View.VISIBLE);
                    address.setVisibility(View.INVISIBLE);

            }
                else if(rb.getId()==specific_loc.getId()){

                    address.setVisibility(View.VISIBLE);
                    reg.setVisibility(View.INVISIBLE);

                }
                else{
                    address.setVisibility(View.INVISIBLE);
                    neighbour.setVisibility(View.INVISIBLE);
                    reg.setVisibility(View.INVISIBLE);
                }
        }

        });


        find = (Button)findViewById(R.id.find);
        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = rg.getCheckedRadioButtonId();
                if(selectedId==current_loc.getId()){
                    Intent i = new Intent(getApplicationContext(),MapsActivity.class);
                    startActivity(i);
                }

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
